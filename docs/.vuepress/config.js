module.exports = {
  base: '/myblob/',
  title: '我的博客',
  description: '我的博客',
  dest: './public',
  head: [
    ['link', { rel: 'icon', href: 'logo.jpeg' }]
  ],
  themeConfig: {
    // 你的GitHub仓库，请正确填写
    repo: 'https://gitlab.com/jianguangjin2/myblob',
    // 自定义仓库链接文字。
    repoLabel: 'My GitLab',
    lastUpdated: 'Last Updated', // string | boolean
    nav: [
      { text: 'Home', link: '/' },
      { text: 'CodeReview', link: '/review/' },
      // { text: '学习', link: '/study/' },
      // { text: '爬虫', link: '/share/' },
      // { text: 'PWA', link: '/pwa/' },
      { text: 'Css', link: '/css/' },

    ],
    sidebar: {
      '/review/': [
        {
          title: '我的项目',
          collapsable: false, // 可选的, 默认值是 true,
          children: [
            {
              title: 'go_up',
              path: 'go_up',
            },
          ]

        }
      ],
      '/study/': [
        {
          title: 'flutter',   // 必要的
          initialOpenGroupIndex: -1, // 可选的, 默认值是 0
          // children: [
          //   {
          //     title: 'js基础',
          //     path: 'js',
          //   },
          //   {
          //     title: 'vue学习',
          //     path: 'vue',
          //   },
          // ]
        },
        {
          title: '算法',   // 必要的
          collapsable: false, // 可选的, 默认值是 true,
        },
        {
          title: 'js',
          collapsable: false,
        },
        {
          title: 'TS',
          collapsable: false,
        },
        {
          title: 'node',
          collapsable: false,
        },
        {
          title: 'webpack',
          collapsable: false,
        },
        {
          title: 'vue',
          collapsable: false,
        },
        {
          title: '小程序',
          collapsable: false,
        },
        {
          title: 'react',
          collapsable: false,
        },
        {
          title: 'es6',
          collapsable: false,
        }
      ],
      '/share/': [
        {
          title: '第一章:网络爬虫（初探）',   // 必要的
          // path: '',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
          collapsable: false, // 可选的, 默认值是 true,
          children: [
            {
              title: '1.1 什么是爬虫',
              path: 'whatsCrawler',
            },
            {
              title: '1.2 python简单实现一个爬虫',
              path: 'creatCrawler',
            },
            {
              title: '1.3 排行榜',
              path: 'rank',
            },
            {
              title: '1.4 Google插件',
              path: 'scraper',
            },
            {
              title: '1.5 第三方采集工具',
              path: 'tools',
            },
          ]
        },
        

      ],
      '/pwa/': [
        {
          title: 'PWA',   // 必要的
          // path: '',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
          collapsable: false, // 可选的, 默认值是 true,
          children: [
            {
              title: '1.0 愿景',
              path: 'pwa00',
            },
            {
              title: '1.1 PWA的由来',
              path: 'pwa01',
            },
            {
              title: '1.2 什么是PWA',
              path: 'pwa02',
            },
            {
              title: '1.3 PWA的发展',
              path: 'pwa03',
            },
            {
              title: '1.4 你发现了啥捏',
              path: 'pwa04',
            },
            {
              title: '1.5 缓存',
              path: 'pwa05',
            },
            {
              title: '1.6 SW简介',
              path: 'pwa06',
            },
            {
              title: '1.7 code',
              path: 'pwa07',
            },
          ]
        },
        

      ],
      '/css/': [
        {
          title: 'css',   // 必要的
          collapsable: false, // 可选的, 默认值是 true,
          children: [
            {
              title: '1.0 毛玻璃',
              path: 'css1',
            },
            {
              title: '1.1 渐变',
              path: 'css2',
            },
            {
              title: '1.2 发光 ',
              path: 'css3',
            },
            {
              title: '1.3 遮罩 ',
              path: 'css4',
            },
            {
              title: '1.4 动画 ',
              path: 'css5',
            },
          ]
        },
      ],
    }
  }
}