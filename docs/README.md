---
home: true
heroImage: logo.jpeg
heroText: Aiou
tagline: Aiou
actionText: Lets Go →
actionLink: /share/
features:
  - title: 学习记录
    details: 总结过往学习经验。
  - title: 技术驱动
    details: 享受学习新技术带来的变化。
  - title: 成果
    details: 详细记录学习成果。
footer: MIT Licensed | Copyright © 2021-present Aiou
---
