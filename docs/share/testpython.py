# PyPI 危机：恶意软件包下载超 3 万次、平台再曝 GitHub Actions 高危漏洞
# https://pypi.org/
# 近日，Python 的官方第三方软件存储库 PyPI 中发现了新的恶意库——能够在受感染设备上提取信用卡号和建立后门。
# 为此，8 个含有恶意代码的相关 Python 软件包快速从 PyPI 门户网站上被删除，但即便如此，这些软件包的下载次数也已经超过 3 万次
#  导入urlopen函数
from urllib.request import urlopen
# 导入BeautifulSoup 主要用来解析 html 标签
from bs4 import BeautifulSoup as bf
# 当使用urllib模块访问https网站时，由于需要提交表单，而python3默认是不提交表单的
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
# 请求获取HTML
# html = urlopen("https://jianguangjin2.gitlab.io/myblob/")
html = urlopen("http://www.baidu.com/")
# 用BeautifulSoup解析html
obj = bf(html.read(), 'html.parser')
# 从标签head、title里提取标题
title = obj.head.title
# 使用find_all函数获取所有图片的信息
# pic_info = obj.find_all('img')
# 只提取logo图片的信息
# logo_pic_info = obj.find_all('img')
logo_pic_info = obj.find_all('img', class_="index-logo-src")
# 提取logo图片的链接
logo_url = "https:"+logo_pic_info[0]['src']
# 打印html结构
print(obj)
# 打印链接
print(logo_url)
# 打印标题
print(title)
