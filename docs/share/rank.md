```
import requests
# re模块主要功能是通过正则表达式是用来匹配处理字符串的
import re
# 主要用来解析 html 标签
import bs4
# Selenium是一个用于测试网站的自动化测试工具，支持各种浏览器包括Chrome、Firefox、Safari等主流界面浏览器，同时也支持phantomJS无界面浏览器
# from selenium import webdriver
# # Python中类似 MATLAB 的绘图工具，熟悉 MATLAB 也可以很快的上手 Matplotlib
# from matplotlib import pyplot as plt
# # wordcloud是优秀的词云展示第三方库，以词语为基本单位，通过图形可视化的方式，更加直观和艺术的展示文本
# from wordcloud import WordCloud
# import jieba
# 专门为处理表格和混杂数据
import pandas as pd


def request_url(url):
    headers = {
        'Referer': 'https://movie.douban.com',
        'Host': 'movie.douban.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'
    }
    response = requests.get(url, headers=headers)
    return response.text


def movie_info(html):
    data = []
    soup = bs4.BeautifulSoup(html, 'html.parser')
    items = soup.select('li > div.item')
    for item in items:
        desc_item = item.select('div.info > div.bd > p.quote > span')
        desc = ''
        if desc_item is not None and len(desc_item) > 0:
            desc = desc_item[0].text
        data.append({
            'url': item.select('div.info > div.hd > a')[0]['href'],
            'title': item.select('div.info > div.hd > a > span')[0].text,
            'rank': item.select('div.pic > em')[0].text,
            'score': item.select('div.info > div.bd > div.star > span.rating_num')[0].text,
            'desc': desc,
        })
    return data


if __name__ == '__main__':
    urls = [
        'https://movie.douban.com/top250?start={0}&filter='.format(i * 25) for i in range(10)]
    movie_list = [request_url(url) for url in urls]
    movie_url_list = [movie_info(movie) for movie in movie_list]
    data = []
    for j in movie_url_list:
        for k in j:
            data.append(k)
    print(data)
    df = pd.DataFrame(data)
    df.to_csv("douban_movies.csv", encoding="utf_8_sig", index=False)
```
