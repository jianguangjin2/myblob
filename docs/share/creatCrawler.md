**步骤**

1. 找到需要爬取内容的网页 URL
2. 打开该网页的检查页面（即查看 HTML 代码，按 F12 快捷键即可进入）
3. 在 HTML 代码中找到你要提取的数据
4. 写 python 代码进行网页请求、解析
5. 存储数据

```js
# 导入urllib库的urlopen函数
from urllib.request import urlopen
# 发出请求，获取html
html = urlopen("http://www.baidu.com/")
# 获取的html内容是字节，将其转化为字符串
html_text = bytes.decode(html.read())
# 打印html内容
print(html_text)
```

python 库 BeautifulSoup 为工具来解析上面已经获取的 HTML

安装 python 插件: 代码提示以及调试

安装 Pyright 插件: 提供 Python 的数据类型检查，熟悉 Python 后，能帮你提前发现代码错误

python3 -m pip install bs4

```js
# 导入urlopen函数
from urllib.request import urlopen
# 导入BeautifulSoup
from bs4 import BeautifulSoup as bf
# 请求获取HTML
html = urlopen("https://jianguangjin2.gitlab.io/myblob/")
# 用BeautifulSoup解析html
obj = bf(html.read(),'html.parser')

# 从标签head、title里提取标题
title = obj.head.title
# 使用find_all函数获取所有图片的信息
pic_info = obj.find_all('img')
# 只提取logo图片的信息
// logo_pic_info = obj.find_all('img',class_="index-logo-src")
logo_pic_info = obj.find_all('img')
# 提取logo图片的链接
logo_url = "https:"+logo_pic_info[0]['src']
# 打印html结构
print(obj)
# 打印链接
print(logo_url)
# 打印标题
print(title)
```
