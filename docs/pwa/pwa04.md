- 有独立的应用名称、图标 （Web App Manifest）
- 没有浏览器框架干扰
- 可作为应用卡片出现在多任务管理
- 支持通知推送
- 额外设置应用通知的类型


**Web App Manifest**


JSON 文件中配置 PWA 的相关信息，应用名称、图标、启动方式、背景颜色、主题颜色等等。添加到桌面后，PWA 并不是一个快捷方式，而是能够在系统中作为一个独立的 App 存在；

缩短了用户和站点的距离，用户可以在主屏直达站点；其次是能够让网站具有更加接近 Native App 的体验，具有启动画面、沉浸式浏览体验

```js
{
  "name": "PWA Chapter01 Demo",
  "short_name": "Chapter01 Demo",
  "icons": [
    {
      "src": "assets/images/icons/icon_144x144.png",
      "sizes": "144x144",
      "type": "image/png"
    },
    {
      "src": "assets/images/icons/icon_152x152.png",
      "sizes": "152x152",
      "type": "image/png"
    },
    {
      "src": "assets/images/icons/icon_192x192.png",
      "sizes": "192x192",
      "type": "image/png"
    },
    {
      "src": "assets/images/icons/icon_512x512.png",
      "sizes": "256x256",
      "type": "image/png"
    }
  ],
  "start_url": "/index.html",
  "display": "standalone",
  "background_color": "#fff",
  "theme_color": "#1976d2"
}
```
