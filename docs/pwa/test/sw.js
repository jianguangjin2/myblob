//self 为 Service Worker 线程的全局命名空间，类似于主线程的 window，在 sw.js 中是访问不到 window 命名空间的。
// sw.js
console.log('service worker 注册成功')

self.addEventListener('install', (event) => {
  // 一段一定会报错的代码
  //  console.log(a.cc)

  // 引入 event.waitUntil 方法
  // event.waitUntil(new Promise((resolve, reject) => {
  //   // 模拟 promise 返回错误结果的情况
  //   reject('安装出错')
  //   // resolve('安装成功')
  // }))
  // self.skipWaiting();
  // 安装回调的逻辑处理
  console.log('service worker 安装成功')
})

self.addEventListener('activate', () => {
  // 激活回调的逻辑处理
  console.log('service worker 激活成功')
})

self.addEventListener('fetch', event => {
  console.log('service worker 抓取请求成功: ' + event.request.url)
})