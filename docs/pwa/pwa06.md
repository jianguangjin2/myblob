
**缘由**

 JavaScript => 单线程（耗资源、时间问题） => Web Worker（postMessage通知主线程、 onmessage 得到反馈，释放性能压力） => 每次访问重复执行、不能持久存在 => Service Worker(生命周期特性保证复杂的工作只处理一次，并持久缓存处理结果，直到修改了 Service Worker 的内在的处理逻辑)


 Appcache(HTML被缓存、维护成本、动态请求无法处理 被废弃) => Service Worker

 **特点**:

- 一个特殊的 worker 线程，独立于当前网页主线程，有自己的执行上下文
- 一旦被安装，就永远存在，除非显示取消注册
- 使用到的时候浏览器会自动唤醒，不用的时候自动休眠
- 可拦截并代理请求和处理返回，可以操作本地缓存，如 CacheStorage，IndexedDB 等
- 离线内容开发者可控
- 能接受服务器推送的离线消息
- 异步实现，内部接口异步化基本是通过 Promise 实现
- 不能直接操作 DOM
- 必须在 HTTPS 环境下才能工作

**注册**

```js
<!DOCTYPE html>
  <head>
    <title>Service Worker Demo</title>
  </head>
  <body>
    <script>
      navigator.serviceWorker.register('./sw.js')
    </script>
  </body>
</html>
//HTML5 API navigator.serviceWorker.register() 方法就能够注册一个 Service Worker
```



**生命周期**

!['生命周期'](./assets/service_worker_lifecycle.png)

注册 => 下载执行service worker => 安装 => install回调（缓存读写的工作）=> activate => 监听 fetch 事件 => 更新、注销unregister => Terminated终结状态

```js
// sw.js

html
img 加载张图片

console.log('service worker 注册成功')

self.addEventListener('install', () => {
  // 安装回调的逻辑处理
  console.log('service worker 安装成功')
})

self.addEventListener('activate', () => {
  // 激活回调的逻辑处理
  console.log('service worker 激活成功')
})

self.addEventListener('fetch', event => {
  console.log('service worker 抓取请求成功: ' + event.request.url)
})

```
**Service Worker 工作流程图**

!['工作流程'](./assets/service_worker_process.png)


self 为 Service Worker 线程的全局命名空间，类似于主线程的 window，在 sw.js 中是访问不到 window 命名空间的。


**waitUntil 机制**

保证生命周期的执行

**终端**

每打开一个页面http://127.0.0.1:5500/的浏览器标签都是一个终端

**clients.claim()**

```js
self.addEventListener('activate', event => {
  event.waitUntil(
    self.clients.claim()
      .then(() => {
        // 返回处理缓存更新的相关事情的 Promise
      })
  )
})
```

保证 Service Worker 激活之后能够马上作用于所有的终端


**更新流程**

!['更新流程'](./assets/service_worker_update_process.png)

3种方式

- 关闭所有终端

- 手动skip

- skipWaiting()

**skipWaiting**

帮助用户快速体验站点升级变化