**Service Worker**
 本质上也是浏览器缓存资源用的;

 - 拦截网络请求
 - 接受服务器推送的离线消息


**http缓存**

Expires：服务器使用Expires头来告诉Web客户端它可以使用当前副本，直到指定的时间为止。

Cache-Control：HTTP1.1引入了Cathe-Control，它使用max-age指定资源被缓存多久，主要是解决了Expires一个重大的缺陷，就是它设置的是一个固定的时间点，客户端时间和服务端时间可能有误差（强缓存）。

Last-Modified / If-Modified-Since：Last-Modified是服务器告诉浏览器该资源的最后修改时间，If-Modified-Since是请求头带上的，上次服务器给自己的该资源的最后修改时间。然后服务器拿去对比。若资源的最后修改时间大于If-Modified-Since，说明资源又被改动过，则响应整片资源内容，返回状态码200；若资源的最后修改时间小于或等于If-Modified-Since，说明资源无新修改，则响应HTTP 304，告知浏览器继续使用当前版本。

Etag / If-None-Match：前面提到由文件的修改时间来判断文件是否改动，还是会带来一定的误差，比如注释等无关紧要的修改等。所以推出了新的方式。Etag是由服务端特定算法生成的该文件的唯一标识，而请求头把返回的Etag值通过If-None-Match再带给服务端，服务端通过比对从而决定是否响应新内容。这也是304缓存。

**浏览器缓存**

Storage：cookie、localStorage、sessionStorage

**前端数据库**

WebSql、IndexDB、Sqflite

**应用缓存**

简称为appcache。Appcache就是从浏览器的缓存中分出来的一块缓存区。可以使用一个描述文件（manifest file），列出要下载和缓存的资源，这样就能在这个缓存中保存数据了。

```js

CACHE MANIFEST
#  第一行必须为CACHE MANIFEST
#  Version V1
CACHE:
#  需要缓存的都放在CACHE:下面
b.css
NETWORK:
#  需要总是从网络请求的放在NETWORK：下面
a.css

FALLBACK:
/404.html
# 请求失败时需要跳转的页面

<DOCTYPE HTML>
<html manifest="demo.appcache">
</html>

if (window.applicationCache) {
    //浏览器支持离线应用
}
```

缺陷:

应用缓存主要是通过manifest文件来注册被缓存的静态资源，他在缓存静态文件的同时，也会默认缓存html文件。这导致页面的更新只能通过修改manifest文件。W3C废弃






