**backdrop-filter CSS 属性可以让你为一个元素后面区域添加图形效果（如模糊或颜色偏移）。 因为它适用于元素背后的所有元素，为了看到效果，必须使元素或其背景至少部分透明**

```js

/* 关键词值 */
backdrop-filter: none;
/* <filter-function> 滤镜函数值 */
backdrop-filter: blur(2px);  //blur可以设置图片使用高斯模糊效果，单位值是px。所谓高斯模糊，就是指一个图像与二维高斯分布的概率密度函数做卷积
backdrop-filter: brightness(2);//brightness可以改变图片的亮度，默认值为100%,即1。
backdrop-filter: contrast(40%);//contrast代表对比度，这个属性取值和饱和度saturate类似，取值也是数字
backdrop-filter: drop-shadow(4px 4px 10px blue);//drop-shadow这个属性类似于box-shadow，给图片加阴影。
backdrop-filter: grayscale(30%); //grayscale代表灰度，取值在0-1之间
backdrop-filter: hue-rotate(120deg);//hue-rotate用来改变图片的色相，默认值为0deg，取值是角度（angle）
backdrop-filter: invert(70%); //invert 可以设定反色， 值为0-1之间的小数
backdrop-filter: sepia(90%);//sepia代表的是照片褐色，类似于大部分美图软件里的怀旧功能的那种效果，取值也是0-1，和grayscale一样
backdrop-filter: saturate(500%);//saturate可以设定照片饱和度，取值范围为数字即可，默认值1，即100%

/* 全局值 */
backdrop-filter: inherit;
backdrop-filter: initial;
backdrop-filter: unset;


```