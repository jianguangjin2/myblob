
**object-fit CSS 属性指定可替换元素（iframe、video、embed、img）的内容应该如何适应到其使用的高度和宽度确定的框。**

- contain
- 被替换的内容将被缩放，以在填充元素的内容框时保持其宽高比。 整个对象在填充盒子的同时保留其长宽比，因此如果宽高比与框的宽高比不匹配，该对象将被添加“黑边”。
- cover
- 被替换的内容在保持其宽高比的同时填充元素的整个内容框。如果对象的宽高比与内容框不相匹配，该对象将被剪裁以适应内容框。
- fill
- 被替换的内容正好填充元素的内容框。整个对象将完全填充此框。如果对象的宽高比与内容框不相匹配，那么该对象将被拉伸以适应内容框。
- none
- 被替换的内容将保持其原有的尺寸。
- scale-down
- 内容的尺寸与 none 或 contain 中的一个相同，取决于它们两个之间谁得到的对象尺寸会更小一些。

**mix-blend-mode CSS 属性描述了元素的内容应该与元素的直系父元素的内容和元素的背景如何混合。**

```js
mix-blend-mode: normal; //正常
mix-blend-mode: multiply; //正片叠底
mix-blend-mode: screen; //滤色
mix-blend-mode: overlay;//叠加
mix-blend-mode: darken;//变暗
mix-blend-mode: lighten;//变亮
mix-blend-mode: color-dodge//颜色减淡
mix-blend-mode: color-burn;//颜色加深
mix-blend-mode: hard-light;//强光
mix-blend-mode: soft-light;//柔光
mix-blend-mode: difference;//差值
mix-blend-mode: exclusion;//排除
mix-blend-mode: hue;//色相
mix-blend-mode: saturation;//饱和度
mix-blend-mode: color;//颜色
mix-blend-mode: luminosity;//亮度

mix-blend-mode: initial;//初始
mix-blend-mode: inherit;//继承
mix-blend-mode: unset; //复原
```