**text-shadow为文字添加阴影。可以为文字与  text-decorations  添加多个阴影，阴影值之间用逗号隔开。每个阴影值由元素在X和Y方向的偏移量、模糊半径和颜色值组成**

**animation-fill-mode 属性可以接受 none | forwards | backwards | both 四个值中的一个**

| mode            |  效果  | 
| ---------------- | :----: |
| none | 不改变默认行为 | 
| forwards | forwards当动画完成后，保持最后一帧的状态（也就是最后一个关键帧中定义的状态） | 
| backwards | 在animation-delay指定动画延迟的一段时间内，元素保持为第一帧中的状态（也就是第一帧中所定义的状态） | 
| both | 表示上面两者模式都被应用 | 
